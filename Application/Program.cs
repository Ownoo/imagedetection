﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.Threading;

namespace Application
{
    class Program
    {
        public static void Main(string[] args)
        {
            // capture from webcam until key pressed
            while (!Console.KeyAvailable)
            {
                var capture = new VideoCapture(0);
                using (var mat = new Mat())
                {
                    capture.Read(mat);
                    if (!mat.IsEmpty)
                    {
                        var processed = Detect(mat, out var found);
                        if (found)
                        {
                            Console.WriteLine("Image saved");
                            processed.Save($"Capture{DateTime.Now:yyyyMMddHHmmssffff}.jpg");
                        }
                        else
                        {
                            Console.WriteLine("No face");
                        }
                    }
                }
                Thread.Sleep(50);
            }
        }


        private static Mat Detect(Mat mat, out bool found)
        {
            found = false;
            using (var eyeClassifier = new CascadeClassifier("haarcascade_eye.xml"))
            using (var faceClassifier = new CascadeClassifier("haarcascade_frontalface_default.xml"))
            {
                using (var ugray = new UMat())
                {
                    CvInvoke.CvtColor(mat, ugray, ColorConversion.Bgr2Gray);
                    CvInvoke.EqualizeHist(ugray, ugray);
                    // detect faces in image
                    var faces = faceClassifier.DetectMultiScale(ugray, 1.1, 10, new Size(20, 20));
                    foreach (var face in faces)
                    {
                        //detect eyes
                        using (var faceRegion = new UMat(ugray, face))
                        {
                            var eyes = eyeClassifier.DetectMultiScale(faceRegion, 1.1, 10, new Size(20, 20));
                            if (eyes.Length > 1)
                            {
                                foreach (var eye in eyes)
                                {
                                    var eyeRectangle = eye;
                                    eyeRectangle.Offset(face.X, face.Y);
                                    // mark up eyes on image
                                    CvInvoke.Rectangle(mat, eyeRectangle, new Bgr(Color.Green).MCvScalar, 2);
                                }
                                // mark up face on image
                                CvInvoke.Rectangle(mat, face, new Bgr(Color.Yellow).MCvScalar, 2);
                                found = true;
                            }
                        }
                    }
                }
            }
            return mat;
        }


        //public static void Main(string[] args)
        //{
        //    if (args.Length > 0)
        //    {
        //        // load image from disk
        //        var mat = new Mat(args[0], ImreadModes.Color);
        //        var processed = Detect(mat);
        //        processed.Save($"Capture{DateTime.Now:yyyyMMddHHmmssffff}.jpg");
        //    }
        //    else
        //    {
        //        // capture from webcam until key pressed
        //        while (!Console.KeyAvailable)
        //        {
        //            var capture = new VideoCapture(0);
        //            if (capture != null)
        //            {
        //                using (var mat = new Mat())
        //                {
        //                    capture.Read(mat);
        //                    if (!mat.IsEmpty)
        //                    {
        //                        var processed = Detect(mat);
        //                        processed.Save($"Capture{DateTime.Now:yyyyMMddHHmmssffff}.jpg");
        //                    }
        //                }
        //            }
        //            Thread.Sleep(50);
        //        }
        //    }
        //}
    }
}